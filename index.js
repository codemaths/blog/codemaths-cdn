var express = require('express');
var app = express();


app.use('/content', express.static('public'));

app.get('/', function (req, res) {
    res.sendFile(__dirname + '/index.html');
});


app.listen(4114, function () {
    console.log('Express app is running in port 4114!');
});